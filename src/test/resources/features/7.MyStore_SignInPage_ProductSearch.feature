@web
@SignUP_SignIn
@Firefox
@postive_case
Feature: As MyStore Logged in User able to search products
Scenario Outline: Verify that user able to SignIn 
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate "<username>" User


Examples:
|url|Browser|email|pasword|username|
|http://automationpractice.com/|firefox|test400@test.com|test@543|MyStore User|

@Firefox
@Product_Search
Scenario Outline: User able to see products Available and verify the counts of results by entering combination of (positive,negative cases) using Firefox Browser
Given I Go to "<url>" on "<Browser>"
Then I Enter Keyword to Search myStore Page "<key>"
And I Click Search Button for myStore Page
Then I Verify Product Count myStore Page "<results>"
And I Click on logout from mystore

Examples: 
|url|Browser|key|results|
|http://automationpractice.com/|firefox|""|0 results have been found.|
|http://automationpractice.com/|firefox|James|0 results have been found.|
|http://automationpractice.com/|firefox|423rwfsr|0 results have been found.|
|http://automationpractice.com/|firefox||0 results have been found.|
|http://automationpractice.com/|firefox|Sleeve|4 results have been found.|
|http://automationpractice.com/|firefox|T-Shirt|1 result has been found.|
|http://automationpractice.com/|firefox|t-Shirt|1 result has been found.|
|http://automationpractice.com/|firefox|T-shirt|1 result has been found.|
|http://automationpractice.com/|firefox|t-shirt|1 result has been found.|
|http://automationpractice.com/|firefox|T-SHIRT|1 result has been found.|
|http://automationpractice.com/|firefox|T|1 result has been found.|
|http://automationpractice.com/|firefox|men|0 result has been found.|
|http://automationpractice.com/|firefox|women|0 result has been found.|  
|http://automationpractice.com/|firefox|dress|7 result has been found.|
   
Scenario: Validate search key  Close Firefox
Then I Close Browser

@Chrome
@Product_Search
Scenario Outline: Verify that user able to SignIn 
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate "<username>" User


Examples:
|url|Browser|email|pasword|username|
|http://automationpractice.com/|chrome|test400@test.com|test@543|MyStore User|



@productsearch
@chrome
Scenario Outline: User able to see products Available and verify the counts of results by entering combination of (positive,negative cases) using Chrome Browser
Given I Go to "<url>" on "<Browser>"
Then I Enter Keyword to Search myStore Page "<key>"
And I Click Search Button for myStore Page
Then I Verify Product Count myStore Page "<results>"
And I Click on logout from mystore

Examples: 
|url|Browser|key|results|
|http://automationpractice.com/|chrome|""|0 results have been found.|
|http://automationpractice.com/|chrome|James|0 results have been found.|
|http://automationpractice.com/|chrome|423rwfsr|0 results have been found.|
|http://automationpractice.com/|chrome||0 results have been found.|
|http://automationpractice.com/|chrome|Sleeve|4 results have been found.|
|http://automationpractice.com/|chrome|T-Shirt|1 result has been found.|
|http://automationpractice.com/|chrome|t-Shirt|1 result has been found.|
|http://automationpractice.com/|chrome|T-shirt|1 result has been found.|
|http://automationpractice.com/|chrome|t-shirt|1 result has been found.|
|http://automationpractice.com/|chrome|T-SHIRT|1 result has been found.|
|http://automationpractice.com/|chrome|T|1 result has been found.|

      

Scenario: Validate search key  Close Firefox
Then I Close Browser
