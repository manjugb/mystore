@web
@Firefox
@user_signin
Feature: SignIn,ProductSearch and AddCart
Scenario Outline: As a MyStore User should be able to SignIn 
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate "<username>" User

@data_user_sign
Examples:
|url|Browser|email|pasword|username|
|http://automationpractice.com/|firefox|used001824@test.com|tes!@3675|veu rsi|

@user_productsearch_add_cart
Scenario Outline: I should see products Available and click on one product then to add Cart
Given I Go to "<url>" on "<Browser>"
Then I Enter Keyword to Search myStore Page "<key>"
And I Click Search Button for myStore Page
Then I Verify Product Count myStore Page "<results>"
And I Click on Product "<productname>" Searched
And I Click Add Cart to Check Out
Then I Look for Product MyStorePrice as "<mystoreprice>" && OldPrice as "<oldprice>"
And I Click Add Cart to Check Out
Then I Verify Icon Ok
Then I Look for totalProductPrice as "<tprice>" && shppingCost as "<shippingCost>"
And I Click on Continue Shoping to add more
And I like to review cart before checkout


Examples: 
|url|Browser|key|results|productname|mystoreprice|oldprice|tprice|shippingCost|
|http://automationpractice.com/|firefox|Printed Summer Dress|1 results have been found.|Printed Summer Dress|$28.98|$30.51|$28.98|$2.00|
|http://automationpractice.com/|firefox|Printed Chiffon Dress|1 results have been found.|Printed Chiffon Dress|$16.40|$20.50|$45.38|$2.00|
|http://automationpractice.com/|firefox|Faded Short Sleeve T-shirts|1 results have been found.|Faded Short Sleeve T-shirts|$16.51|$0.0|$61.89|$2.00|


@go_to_shopping
@Firefox
Scenario Outline: I want to add available products into add Cart
Given I Go to "<url>" on "<Browser>"
Then I Click on Checkout Proceed Button
Then I Click on logout from mystore

Examples: 
|url|Browser|
|http://automationpractice.com/|firefox|

