@web
@SignUP_Create_Account
@Firefox
@Chrome
Feature: I will create with my existing email,new email,invalid formats
Scenario Outline: I Should get valid error message An account has been already register.Please enter valid password or create new one and handle invalid inputs
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Verify h3 title "<h3title>" 
And I Verify label of Email "<label>"
And I Create Account with email "<email>"
Then I Verify Error "<errmsg>" Messgae

     
Examples:
|url|Browser|h3title|label|email|errmsg|
|http://automationpractice.com/|firefox|CREATE AN ACCOUNT|Email address|jamesbond|Invalid email address.|
|http://automationpractice.com/|firefox|CREATE AN ACCOUNT|Email address|jamesbond@test.com|An account using this email address has already been registered. Please enter a valid password or request a new one.|
|http://automationpractice.com/|firefox|CREATE AN ACCOUNT|Email address||Invalid email address.|
|http://automationpractice.com/|firefox|CREATE AN ACCOUNT|Email address|""|Invalid email address.|
|http://automationpractice.com/|firefox|CREATE AN ACCOUNT|Email address|testuser@test.com|An account using this email address has already been registered. Please enter a valid password or request a new one.|
|http://automationpractice.com/|firefox|CREATE AN ACCOUNT|Email address|user077@test.com|An account using this email address has already been registered. Please enter a valid password or request a new one.|


Scenario: Close Firefox 
Then I Close Browser

Scenario Outline: I Should valid error message An account has been already register.Please enter valid password or create new one
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Verify h3 title "<h3title>" 
And I Verify label of Email "<label>"
And I Create Account with email "<email>"
Then I Verify Error "<errmsg>" Messgae
Then I Close Browser
     
Examples:
|url|Browser|h3title|label|email|errmsg|
|http://automationpractice.com/|chrome|CREATE AN ACCOUNT|Email address|jamesbond|Invalid email address.|
|http://automationpractice.com/|chrome|CREATE AN ACCOUNT|Email address|jamesbond@test.com|An account using this email address has already been registered. Please enter a valid password or request a new one.|
|http://automationpractice.com/|chrome|CREATE AN ACCOUNT|Email address||Invalid email address.|
|http://automationpractice.com/|chrome|CREATE AN ACCOUNT|Email address|""|Invalid email address.|
|http://automationpractice.com/|chrome|CREATE AN ACCOUNT|Email address|testuser@test.com|An account using this email address has already been registered. Please enter a valid password or request a new one.|
|http://automationpractice.com/|chrome|CREATE AN ACCOUNT|Email address|user077@test.com|An account using this email address has already been registered. Please enter a valid password or request a new one.|


Scenario: Close Chrome
Then I Close Browser
     


