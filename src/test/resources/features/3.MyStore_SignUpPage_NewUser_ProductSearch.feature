@web
@SignUP_Create_Account_Register
@Firefox
Feature: New Registration I would like to become myStore user and need to registered
Scenario Outline: As New user check my user already registered or not
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Verify h3 title "<h3title>" 
And I Verify label of Email "<label>"
And I Create Account with email "<email>"
And I Enter "<fname>" as FirstName to register
And I Enter "<lname>" as LastName to register
And I Enter "<pswrd>" as Password to register
And I Enter "<cfname>" as Custmer FirstName to register
And I Enter "<clname>" as Custmer LastName to register
And I Enter "<custadd>" as Custmer Address to register
And I Enter "<city>" as Custmer City to register
And I Enter "<postcode>" as Custmer PostCode to register
And I Select "<state>" as Custmer State to register
And I Enter "<mobile>" as Custmer mobile to register
Then I Click on Register On Personal Information Page
Then I Validate "<username>" User


Examples:
|url|Browser|h3title|label|email|fname|lname|pswrd|cfname|clname|custadd|postcode|city|state|mobile|username|
|http://automationpractice.com/|firefox|CREATE AN ACCOUNT|Email address|used018074@test.com|dveu|rsti|tes!@3675|cfname|claname|93/5,Bolivard Avenue|45307|NYPD|Delaware|23567179|veu rsti|


@user_productsearch_add_cart
Scenario Outline: I should see products Available and click on one product then to add Cart
Given I Go to "<url>" on "<Browser>"
Then I Enter Keyword to Search myStore Page "<key>"
And I Click Search Button for myStore Page
Then I Verify Product Count myStore Page "<results>"
And I Click on Product "<productname>" Searched
And I Click Add Cart to Check Out
Then I Look for Product MyStorePrice as "<mystoreprice>" && OldPrice as "<oldprice>"
And I Click Add Cart to Check Out
Then I Verify Icon Ok
Then I Look for totalProductPrice as "<tprice>" && shppingCost as "<shippingCost>"
And I Click on Continue Shoping to add more
And I like to review cart before checkout


Examples: 
|url|Browser|key|results|productname|mystoreprice|oldprice|tprice|shippingCost|
|http://automationpractice.com/|firefox|Printed Summer Dress|1 results have been found.|Printed Summer Dress|$28.98|$30.51|$28.98|$2.00|
|http://automationpractice.com/|firefox|Printed Chiffon Dress|1 results have been found.|Printed Chiffon Dress|$16.40|$20.50|$45.38|$2.00|
|http://automationpractice.com/|firefox|Faded Short Sleeve T-shirts|1 results have been found.|Faded Short Sleeve T-shirts|$16.51|$0.0|$61.89|$2.00|


@go_to_shopping
@Firefox
Scenario Outline: I want to add available products into add Cart
Given I Go to "<url>" on "<Browser>"
Then I Click on Checkout Proceed Button
Then I Click on logout from mystore

Examples: 
|url|Browser|
|http://automationpractice.com/|firefox|



Scenario: Validate search key  Close Firefox
Then I Close Browser

