@web
@SignUP_SignIn
@Firefox
@negative_case
Feature: As MyStore User I forgot my SingIn Credentials would like to do login Attempts
Scenario Outline: I would like try different combinations of email,password values and SigIn see different errors.
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate Error "<errormsg>" Message


Examples:
|url|Browser|email|pasword|errormsg|
|http://automationpractice.com/|firefox|||An email address required.|
|http://automationpractice.com/|firefox|test123@test.com||An email address required.|
|http://automationpractice.com/|firefox|test123@test.com||Password is required.|
|http://automationpractice.com/|firefox||test@456|An email address required.|
|http://automationpractice.com/|firefox|test123@test.com|test@456|An email address required.|
|http://automationpractice.com/|firefox|jamesbond@test.com|test@456|An email address required.|

Scenario: Validate search key  Close Firefox
Then I Close Browser

@Chrome
Scenario Outline: Verify that user able to SignIn 
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate Error "<errormsg>" Message


Examples:
|url|Browser|email|pasword|errormsg|
|http://automationpractice.com/|chrome|||An email address required.|
|http://automationpractice.com/|chrome|test123@test.com||An email address required.|
|http://automationpractice.com/|chrome|test123@test.com||Password is required.|
|http://automationpractice.com/|chrome||test@456|An email address required.|
|http://automationpractice.com/|chrome|test123@test.com|test@456|An email address required.|

Scenario: Validate search key  Close chrome
Then I Close Browser
