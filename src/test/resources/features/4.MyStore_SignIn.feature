@web
Feature: As MyStore User would like be SignIn

@SignIn
@Firefox
@postive_case
Scenario Outline: As a MyStore User with Valid Credentials should be able to SignIn with out any disputes
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate "<username>" User
And I Click on logout from mystore

@data
Examples:
|url|Browser|email|pasword|username|
|http://automationpractice.com/|firefox|test400@test.com|test@543|MyStore User|


@SignUP_SignIn
@Firefox
@negative_case
Scenario Outline: As a MyStore User with Invalid Credentials unable SignIn with login error disputes
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate Error "<errormsg>" Message


@data
Examples:
|url|Browser|email|pasword|errormsg|
|http://automationpractice.com/|firefox|||An email address required.|
|http://automationpractice.com/|firefox|test123@test.com||An email address required.|
|http://automationpractice.com/|firefox|test123@test.com||Password is required.|
|http://automationpractice.com/|firefox||test@456|An email address required.|
|http://automationpractice.com/|firefox|test123@test.com|test@456|An email address required.|

Scenario: Validate search key  Close Firefox
Then I Close Browser



@SignUP_SignIn
@Chrome
@postive_case
Scenario Outline:As a MyStore User with Valid Credentials should be able to SignIn with out any disputes 
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate "<username>" User
And I Click on logout from mystore


@data
Examples:
|url|Browser|email|pasword|username|
|http://automationpractice.com/|chrome|test400@test.com|test@543|MyStore User|

@SignUP_SignIn
@Chrome
@negative_case
Scenario Outline: As a MyStore User with Invalid Credentials unable SignIn with login error disputes
Given I Go to "<url>" on "<Browser>"
Then I Click SignIn Link
And I Enter "<email>" as Email to SignIn
And I Enter "<pasword>" as Password to SignIn
Then I Click on SignIn to Submit
Then I Validate Error "<errormsg>" Message


@data
Examples:
|url|Browser|email|pasword|errormsg|
|http://automationpractice.com/|chrome|||An email address required.|
|http://automationpractice.com/|chrome|test123@test.com||An email address required.|
|http://automationpractice.com/|chrome|test123@test.com||Password is required.|
|http://automationpractice.com/|chrome||test@456|An email address required.|
|http://automationpractice.com/|chrome|test123@test.com|test@456|An email address required.|

Scenario: Validate search key  Close Chrome
Then I Close Browser




