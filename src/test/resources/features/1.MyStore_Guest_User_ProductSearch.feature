@web
@productsearch
@Firefox
Feature: As a I would like search on differenent filters for product
Scenario Outline: I should see products available with out login as My Store User
Given I Go to "<url>" on "<Browser>"
Then I Enter Keyword to Search myStore Page "<key>"
And I Click Search Button for myStore Page
Then I Verify Product Count myStore Page "<results>"

Examples: 
|url|Browser|key|results|
|http://automationpractice.com/|firefox|""|0 results have been found.|
|http://automationpractice.com/|firefox|James|0 results have been found.|
|http://automationpractice.com/|firefox|423rwfsr|0 results have been found.|
|http://automationpractice.com/|firefox||0 results have been found.|
|http://automationpractice.com/|firefox|Sleeve|4 results have been found.|
|http://automationpractice.com/|firefox|T-Shirt|1 result has been found.|
|http://automationpractice.com/|firefox|t-Shirt|1 result has been found.|
|http://automationpractice.com/|firefox|T-shirt|1 result has been found.|
|http://automationpractice.com/|firefox|t-shirt|1 result has been found.|
|http://automationpractice.com/|firefox|T-SHIRT|1 result has been found.|
|http://automationpractice.com/|firefox|T|1 result has been found.|
|http://automationpractice.com/|firefox|men|0 result has been found.|
|http://automationpractice.com/|firefox|women|0 result has been found.|  
|http://automationpractice.com/|firefox|dress|7 result has been found.|
|http://automationpractice.com/|firefox|Printed|7 result has been found.|
|http://automationpractice.com/|firefox|Chiffon|7 result has been found.|
|http://automationpractice.com/|firefox|Printed Chiffon Dress |7 result has been found.|
|http://automationpractice.com/|firefox|Printed Chiffon Dress |7 result has been found.|
   
Scenario: Validate search key  Close Firefox
Then I Close Browser

@web
@productsearch
@chrome
Scenario Outline: I should see products available with out login as My Store User.
Given I Go to "<url>" on "<Browser>"
Then I Enter Keyword to Search myStore Page "<key>"
And I Click Search Button for myStore Page
Then I Verify Product Count myStore Page "<results>"

Examples: 
|url|Browser|key|results|
|http://automationpractice.com/|chrome|""|0 results have been found.|
|http://automationpractice.com/|ch|James|0 results have been found.|
|http://automationpractice.com/|chrome|423rwfsr|0 results have been found.|
|http://automationpractice.com/|chrome||0 results have been found.|
|http://automationpractice.com/|chrome|Sleeve|4 results have been found.|
|http://automationpractice.com/|chrome|T-Shirt|1 result has been found.|
|http://automationpractice.com/|chrome|t-Shirt|1 result has been found.|
|http://automationpractice.com/|chrome|T-shirt|1 result has been found.|
|http://automationpractice.com/|chrome|t-shirt|1 result has been found.|
|http://automationpractice.com/|chrome|T-SHIRT|1 result has been found.|
|http://automationpractice.com/|chrome|T|1 result has been found.|
|http://automationpractice.com/|chrome|men|0 result has been found.|
|http://automationpractice.com/|chrome|women|0 result has been found.|  
|http://automationpractice.com/|chrome|dress|7 result has been found.|
|http://automationpractice.com/|chrome|Printed|7 result has been found.|
|http://automationpractice.com/|chrome|Chiffon|7 result has been found.|
|http://automationpractice.com/|chrome|Printed Chiffon Dress |7 result has been found.|
|http://automationpractice.com/|chrome|Printed Chiffon Dress |7 result has been found.|

      

Scenario: Validate search key  Close chrome
Then I Close Browser
