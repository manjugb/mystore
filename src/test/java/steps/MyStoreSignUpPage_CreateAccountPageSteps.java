package steps;

import static utils.Constants.myLogger;

import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Then;
import testactions.MyStoreCommonPageTest;
import testactions.SignUpPage_CreateAccountPageTest;

/**
 * 
 * @author manjunath
 * {@summary} this class defines create account actions as a methods
 */

public class MyStoreSignUpPage_CreateAccountPageSteps {
	
	static WebDriverWait wait;
	private SignUpPage_CreateAccountPageTest signUpPage = new SignUpPage_CreateAccountPageTest();
	
	@Then("^I Click SignIn Link$")
	public void clk_Search() throws Throwable {
		myLogger.info("I click on SignIn button ");
		signUpPage.sign_link();
		MyStoreCommonPageTest.waitForPageToLoad();
		
	}
	
	@Then("^I Verify h3 title \"([^\"]*)\"$")
	public void verify_h3_title(String htitle) throws Throwable {
		
		signUpPage.verify_h3_title_create_account(htitle);
		
	}
	@Then("^I Verify label of Email \"([^\"]*)\"$")
	public void verify_label(String label) throws Throwable {

		signUpPage.verify_label_create_account(label);
		
	}
	
	@Then("^I Create Account with email \"([^\"]*)\"$")
	public void create_Account(String email) throws InterruptedException {
		signUpPage.create_account_email_validation(email);
		
	}
	

	@Then("^I Verify Error \"([^\"]*)\" Messgae$")
	public void verify_error_msg(String email) throws InterruptedException {
		signUpPage.verify_error_msg(email);
		
	}
	
}
