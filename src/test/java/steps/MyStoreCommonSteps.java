package steps;

import static utils.BrowserFactory.wd;
import static utils.Constants.myLogger;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import testactions.MyStoreCommonPageTest;
import testactions.MyStoreProductPageTest;
import utils.BrowserFactory;

public class MyStoreCommonSteps {
	private MyStoreCommonPageTest cpt = new MyStoreCommonPageTest();

	@Given("^I Go to \"([^\"]*)\" on \"([^\"]*)\"$")
	// Given("^I Go to \"([^\"]*)\" on \"([^\"]*)\"$", (String url,String browser)
	// -> {
	public void open_browser(String url, String browser) throws Throwable {
		BrowserFactory.getDriver(url, browser);
		wd.get(url);
		myLogger.info("Mystore home page opened ");
		

	}
	
	
	
	
	
}
