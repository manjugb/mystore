package steps;

import static utils.Constants.myLogger;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.BrowserFactory;

import static utils.BrowserFactory.wd;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import testactions.MyStoreProductPageTest;

/**
 * 
 * @author manjunath
 * {@summary} this class defines StepDifinations of Search Functionality to interact Given,When,Then Gherkin style annoations
 *
 */
public class MyStoreProductPageSteps{
	static WebDriverWait wait;
	private MyStoreProductPageTest mspt = new MyStoreProductPageTest();
	
	

	
	@Then("^I Enter Keyword to Search myStore Page \"([^\"]*)\"$")
	public void enter_key(String key) throws Throwable {
		/**
		 * @description this method enters the input text 
		 */
		// test.log(Status.PASS, "User Enter the keyword to search");
		mspt.enter_key(key);
		myLogger.info(" Guest user enter "+key+" the keyword to search");

	}
	
	@Then("^I Verify Product Count myStore Page \"([^\"]*)\"$")
	public void verify_results(String results) throws Throwable {
		/**
		 * @description this method enters the input text 
		 */
		// test.log(Status.PASS, "User Enter the keyword to search");
		mspt.verifySearchKey(results);
		myLogger.info(" Guest user enter "+results+" the keyword to search ");

	}
	
	/*
	 * @Then("I Press Enter$") public void pres_enter() throws Throwable {
	 * stp.press_enter();
	 * myLogger.info(" Guest user enter the key in order to press"); }
	 */

	@Then("^I Click Search Button for myStore Page$")
	public void clk_Search() throws Throwable {

		mspt.submit_search();
		myLogger.info("User click on search button");
	}
	
	@Then("^I Close Browser$")
	public void close_br() throws Throwable {
		mspt.destroyDriver();
            myLogger.info("Close the browser ");


	}
	
	@And("^I Click on Product \"([^\"]*)\" Searched$")
	public void click_product(String product_name) {
		//wd.findElement(By.linkText(product_name)).click();
		mspt.click_product(product_name);
	}
	
	@Then("^I Check Product Title as \"([^\"]*)\" && Product Condition \"([^\"]*)\"$")
	public void verify_title_condition(String h1title,String condition) {
		mspt.verify_h1_title(h1title);
		mspt.verify_condition_item(condition);
	}
	
	@Then("^I Look for Product MyStorePrice as \"([^\"]*)\" && OldPrice as \"([^\"]*)\"$")
	public void verify_price_discount(String price,String discount) {
		mspt.verify_product_price(price);
		mspt.verify_prod_discount(discount);
	}
	
	@And("^I try to increase item quantity$")
	public void increase_quatity() {
		//wd.findElement(By.linkText(product_name)).click();
		mspt.increase_quantity();
	}
	
	@And("^I would like to dicrease item quantity$")
	public void dicrease_quatity() throws Throwable {
		//wd.findElement(By.linkText(product_name)).click();
		mspt.dicrease_quantity();
	}
	

	
	
	
	
	


}
