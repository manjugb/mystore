package steps;

import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Then;
import testactions.MyStoreProductPageTest;
import testactions.MyStoreSignUpPage_SignInPageTest;

public class MyStoreSignUpPage_SignInPageSteps {
	static WebDriverWait wait;
	private MyStoreSignUpPage_SignInPageTest signIn = new MyStoreSignUpPage_SignInPageTest();
	

	@Then("^I Enter \"([^\"]*)\" as Email to SignIn$")
	public void enter_fname(String email) {
		signIn.enter_email_signIn(email);

	}
	
	@Then("^I Enter \"([^\"]*)\" as Password to SignIn$")
	public void enter_paswrd(String pswrd) {
		signIn.enter_pswrd_signIn(pswrd);

	}
	
	@Then("^I Click on SignIn to Submit$")
	public void click_signin() {
		signIn.submit_signIn();

	}
	
	@Then("^I Validate \"([^\"]*)\" User$")
	public void validate_login_success_user(String user) {
		signIn.check_user_success(user);

	}
	
	@Then("^I Validate Error \"([^\"]*)\" Message$")
	public void validate_login_error(String message) {
		signIn.verify_error_msg(message);

	}
	
	@Then("^I Click on logout from mystore$")
	public void click_logout() {
		signIn.submit_logout();

	}
	
}
