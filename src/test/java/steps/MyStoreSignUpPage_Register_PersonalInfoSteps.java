package steps;

import java.util.List;

import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import testactions.MyStoreCommonPageTest;
import testactions.MyStoreProductPageTest;
import testactions.MyStoreSignUpPage_Register_PersonalInfoTest;

public class MyStoreSignUpPage_Register_PersonalInfoSteps {
	static WebDriverWait wait;
	private MyStoreSignUpPage_Register_PersonalInfoTest registerInfo = new MyStoreSignUpPage_Register_PersonalInfoTest();

	@Then("^I Enter \"([^\"]*)\" as FirstName to register$")
	public void enter_fname(String fname) {

		MyStoreCommonPageTest.waitForPageToLoad();
		registerInfo.enter_firstname(fname);

	}

	@Then("^I Enter \"([^\"]*)\" as LastName to register$")
	public void enter_lname(String lname) {
		registerInfo.enter_lastname(lname);

	}

	@Then("^I Enter \"([^\"]*)\" as Password to register$")
	public void enter_psword(String pswrd) {
		registerInfo.enter_pswrd(pswrd);

	}

	@Then("^I Enter \"([^\"]*)\" as Custmer FirstName to register$")
	public void enter_custfname(String custfname) {
		registerInfo.enter_cus_firtname(custfname);

	}
	
	@Then("^I Enter \"([^\"]*)\" as Custmer LastName to register$")
	public void enter_cust_lanme(String custlname) {
		registerInfo.enter_cus_lname(custlname);

	}
	
	@Then("^I Enter \"([^\"]*)\" as Custmer Address to register$")
	public void enter_cust_add(String custadd) {
		registerInfo.enter_cus_address(custadd);

	}
	


	
	
	@Then("^I Enter \"([^\"]*)\" as Custmer PostCode to register$")
	public void enter_zip(String zip) {
		registerInfo.enter_cus_zip(zip);

	}
	
	
	
	@Then("^I Enter \"([^\"]*)\" as Custmer City to register$")
	public void enter_cust_city(String custcity) {
		registerInfo.enter_cus_city(custcity);

	}
	
	@Then("^I Select \"([^\"]*)\" as Custmer State to register$")
	public void enter_cust_state(String custstate) {
		//registerInfo.select_cus_state(custstate);
		registerInfo.select_state(custstate);

	}
	
	@Then("^I Enter \"([^\"]*)\" as Custmer mobile to register$")
	public void enter_cust_mob(String custmob) {
		registerInfo.enter_mob_phone(custmob);

	}

	//@And("^I Enter Personal Info to Register$")
 /*	public void register(cucumber.api.DataTable personalInfo) throws Throwable {

		// Write the code to handle Data Table
		List<java.util.Map<String, String>> data = personalInfo.asMaps(String.class, String.class);
		enter_firstname(fname);
		enter_lastname(lname);
		enter_pswrd(pswrd);
		enter_cus_firtname(cust_fname);
		enter_cus_lname(cust_lname);
		enter_cus_address(cust_add);
		enter_cus_zip(post_code);
		enter_cus_city(cust_city);
		select_cus_state(cust_state);
		enter_mob_phone(mob_phone);

	}*/

	@Then("^I Click on Register On Personal Information Page")
	public void click_register() {
		registerInfo.register();

	}
}
