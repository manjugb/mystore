package steps;

import java.util.List;

import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import testactions.MyStoreAddCartPageTest;
import testactions.MyStoreProductPageTest;

public class MyStoreAddCartPageTestSteps {

	static WebDriverWait wait;
	private MyStoreAddCartPageTest msapt = new MyStoreAddCartPageTest();
	
	
	@And("^I Click Add Cart to Check Out$")
	public void add_to_cart() throws Throwable{
		msapt.add_to_cart();
		//wd.findElement(By.xpath(".//*[text()='Add to cart']")).click();
	
	}
	
	@And("^I Move over to \"([^\"]*)\" Clcik to AddCart$")
	public void move_over_click(String prod_title) {
		msapt.click_mouse_over_item(prod_title);
	}

	//@And("^I add list of product into add cart$")
	/*public void add_products(String[] items) {
		msapt.AddtoCart(items);
	}*/
	
	@Then("^I Verify Icon Ok$")
	public void verify_icon_ok() {
		//wd.findElement(By.linkText(product_name)).click();
		msapt.check_add_ok_icon();
	}
	
	
	@Then("^I Look for Product color as \"([^\"]*)\" && quantity as \"([^\"]*)\"$")
	public void verify_color_quantity(String color,String quantity) {
		msapt.check_color_item(color);
		msapt.check_quantity(quantity);
	}
	
	@Then("^I Look for totalProductPrice as \"([^\"]*)\" && shppingCost as \"([^\"]*)\"$")
	public void verify_tprice_shcost(String totpprice,String shippingcost) {
		msapt.check_total_product_price(totpprice);
		msapt.check_shipping_cost(shippingcost);
	}
	
	
	@And("^I Click on Checkout Proceed Button$")
	public void preview_precheck_button() throws Throwable {
		//wd.findElement(By.linkText(product_name)).click();
		
		msapt.priview_check_button();
	}
	
	
	@And("^I Click on Continue Shoping to add more$")
	public void continue_precheck_button() throws Throwable {
		//wd.findElement(By.linkText(product_name)).click();
		msapt.continue_shoping_button();
	}
	
	@Then("^I Check Terms & Conditions$")
	public void check_click() throws Throwable {
		//wd.findElement(By.linkText(product_name)).click();
		msapt.check_box_click();
	}
	
	@And("^I like to review cart before checkout$")
	public void view_cart() throws Throwable {
		//wd.findElement(By.linkText(product_name)).click();
		msapt.view_cart_click();
	}
	
	
	
	
	
	
	
}
