package testactions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static utils.Constants.myLogger;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import pageObjects.MyStore_SignUpPage_CreateAccount;
import pageObjects.MyStoreSignUpPage_Register_PersonalInfo;
import utils.BrowserFactory;
import utils.Utils;
/**
 * 
 * @author manjunath
 * {@summary} this class defines Personal Information needed for Registering account my Store after create account.
 */

public class MyStoreSignUpPage_Register_PersonalInfoTest extends BrowserFactory{
	private MyStoreSignUpPage_Register_PersonalInfo RegisterPage = new MyStoreSignUpPage_Register_PersonalInfo();

	public MyStoreSignUpPage_Register_PersonalInfoTest() {
		this.RegisterPage = new MyStoreSignUpPage_Register_PersonalInfo();
		PageFactory.initElements(wd, RegisterPage);
	}
	
	
	
	
	public void enter_firstname(String fname) {
		try {
			if (Utils.existsElement(RegisterPage.elmfirstname)) {
				RegisterPage.elmfirstname.clear();
				RegisterPage.elmfirstname.click();
				RegisterPage.elmfirstname.sendKeys(fname);
				myLogger.info("WebElement " + RegisterPage.elmfirstname + "Exist");
				assertEquals(RegisterPage.elmfirstname.getAttribute("value"), fname);

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.elmfirstname + "Not Exist");
				assertNotEquals(RegisterPage.elmfirstname.getAttribute("value"), fname);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.elmfirstname, "Enterelmfirstname");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			myLogger.error("Error: " + e);

		}
	}
	
	public void enter_lastname(String lname) {
		try {
			if (Utils.existsElement(RegisterPage.elmlastname)) {
				RegisterPage.elmlastname.clear();
				RegisterPage.elmlastname.click();
				RegisterPage.elmlastname.sendKeys(lname);
				myLogger.info("WebElement " + RegisterPage.elmlastname + "Exist");
				assertEquals(RegisterPage.elmlastname.getAttribute("value"), lname);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.elmlastname + "Not Exist");
				assertNotEquals(RegisterPage.elmlastname.getAttribute("value"), lname);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.elmlastname, "Enterelmlastname");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void enter_pswrd(String pswrd) {
		try {
			if (Utils.existsElement(RegisterPage.elmpswrd)) {
				RegisterPage.elmpswrd.clear();
				RegisterPage.elmpswrd.sendKeys(pswrd);
				myLogger.info("WebElement " + RegisterPage.elmpswrd + "Exist");
				assertEquals(RegisterPage.elmpswrd.getAttribute("value"), pswrd);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.elmpswrd + "Not Exist");
				assertNotEquals(RegisterPage.elmlastname.getAttribute("value"), pswrd);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.elmpswrd, "EnterPasswrd");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void enter_cus_firtname(String cust_firsname) {
		try {
			if (Utils.existsElement(RegisterPage.customer_firstname)) {
				RegisterPage.customer_firstname.clear();
				RegisterPage.customer_firstname.click();
				RegisterPage.customer_firstname.sendKeys(cust_firsname);
				myLogger.info("WebElement " + RegisterPage.customer_firstname + "Exist");
				assertEquals(RegisterPage.customer_firstname.getAttribute("value"), cust_firsname);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.customer_firstname + "Not Exist");
				assertNotEquals(RegisterPage.customer_firstname.getAttribute("value"), cust_firsname);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.customer_firstname, "EnterPasswrd");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void enter_cus_lname(String cust_lname) {
		try {
			if (Utils.existsElement(RegisterPage.customer_lastname)) {
				RegisterPage.customer_lastname.clear();
				RegisterPage.customer_lastname.click();
				RegisterPage.customer_lastname.sendKeys(cust_lname);
				myLogger.info("WebElement " + RegisterPage.customer_lastname + "Exist");
				assertEquals(RegisterPage.customer_lastname.getAttribute("value"), cust_lname);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.customer_lastname + "Not Exist");
				assertNotEquals(RegisterPage.customer_lastname.getAttribute("value"), cust_lname);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.customer_lastname, "cust_lname");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void enter_cus_address(String cust_address) {
		try {
			if (Utils.existsElement(RegisterPage.customer_address)) {
				RegisterPage.customer_address.clear();
				RegisterPage.customer_address.click();
				RegisterPage.customer_address.sendKeys(cust_address);
				myLogger.info("WebElement " + RegisterPage.customer_address + "Exist");
				assertEquals(RegisterPage.customer_address.getAttribute("value"), cust_address);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.customer_address + "Not Exist");
				assertNotEquals(RegisterPage.customer_address.getAttribute("value"), cust_address);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.customer_address, "cust_address");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void enter_cus_city(String cust_city) {
		try {
			if (Utils.existsElement(RegisterPage.customer_city)) {
				RegisterPage.customer_city.clear();
				RegisterPage.customer_city.click();
				RegisterPage.customer_city.sendKeys(cust_city);
				myLogger.info("WebElement " + RegisterPage.customer_city + "Exist");
				assertEquals(RegisterPage.customer_city.getAttribute("value"), cust_city);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.customer_city + "Not Exist");
				assertNotEquals(RegisterPage.customer_city.getAttribute("value"), cust_city);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.customer_city, "cust_city");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void enter_cus_zip(String post_code) {
		try {
			if (Utils.existsElement(RegisterPage.postcode)) {
				RegisterPage.postcode.clear();
				RegisterPage.postcode.click();
				RegisterPage.postcode.sendKeys(post_code);
				myLogger.info("WebElement " + RegisterPage.postcode + "Exist");
				assertEquals(RegisterPage.postcode.getAttribute("value"), post_code);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.postcode + "Not Exist");
				assertNotEquals(RegisterPage.postcode.getAttribute("value"), post_code);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.postcode, "postcode");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	//
	
	public void select_state(String city) {
		// WebElement select = wd.findElement(By.id("gender"));
		WebElement select = RegisterPage.select_state;

		List<WebElement> options = select.findElements(By.tagName("option"));

		for (WebElement option : options) {

			if (city.equals(option.getText()))

				option.click();

		}
	}

	// Actions actionList = new Actions(wd);
	//actionList.clickAndHold( RegisterPage.select_state).sendKeys(city);
	//release().build().perform();
	
	
	public void select_cus_state(String cust_state) {
		try {
			if (Utils.existsElement(RegisterPage.select_state)) {
				//new Select(wd.findElement(By.id("blood-group"))).selectByVisibleText("AB");
				Select select = new Select(RegisterPage.select_state);
				select.selectByVisibleText(cust_state);
				//wd.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
				myLogger.info("WebElement " + RegisterPage.select_state + "Exist");
				assertEquals(RegisterPage.select_state.getAttribute("value"), cust_state);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.select_state + "Not Exist");
				assertNotEquals(RegisterPage.select_state.getAttribute("value"), cust_state);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.select_state, "cust_state");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	
	
	
	public void enter_mob_phone(String mob_phone) {
		try {
			if (Utils.existsElement(RegisterPage.mobile_phone)) {
				RegisterPage.mobile_phone.clear();
				RegisterPage.mobile_phone.click();
				RegisterPage.mobile_phone.sendKeys(mob_phone);
				myLogger.info("WebElement " + RegisterPage.mobile_phone + "Exist");
				assertEquals(RegisterPage.mobile_phone.getAttribute("value"), mob_phone);
				

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.mobile_phone + "Not Exist");
				assertNotEquals(RegisterPage.mobile_phone.getAttribute("value"), mob_phone);
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.mobile_phone, "mob_phone");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	
	
	
	
	public void register(){
		try {
			if (Utils.existsElement(RegisterPage.Register)) {
				
				RegisterPage.Register.click();
				myLogger.info("Customer is susscessfully Registerd with My Store Page");

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + RegisterPage.register_error + "Not Exist");
				utils.Utils.screenshotLocatorWrite(wd, RegisterPage.register_error, "mob_phone");
				
				myLogger.info("Customer is not Register" +RegisterPage.register_error+" ");
			
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			utils.Utils.screenshotLocatorWrite(wd,"register");

		}
	}
	
}
