package testactions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static utils.Constants.myLogger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.MyStoreAddCartPage;
import utils.BrowserFactory;
import utils.Utils;

public class MyStoreAddCartPageTest extends BrowserFactory {
	private static MyStoreAddCartPage addCartPage = new MyStoreAddCartPage();

	public MyStoreAddCartPageTest() {
		this.addCartPage = new MyStoreAddCartPage();
		PageFactory.initElements(wd, addCartPage);
	}

	public void add_to_cart() {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(addCartPage.addtoCart)) {
				
				Actions actions = new Actions(wd);
				//actions.moveToElement(addCartPage.addtoCart);
				actions.moveToElement(addCartPage.addtoCart).click().perform();
				//actions.click(addCartPage.addtoCart);
				//addCartPage.addtoCart.click();
				//MyStoreCommonPageTest.waitForPageToLoad();
				myLogger.info(" " + addCartPage.addtoCart + "Exist");
				//assertEquals(addCartPage.addtoCart.getAttribute("value"), addCartPage.addtoCart);
				utils.Utils.clickElementThroughJavaScript(wd, addCartPage.addtoCart );
				//JavascriptExecutor js = (JavascriptExecutor) wd;
				//js.executeScript("arguments[0].click();", firstbutton);
				//js.executeScript("var evt = document.createEvent('MouseEvents');" + "evt.initMouseEvent('click',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);" + "arguments[0].dispatchEvent(evt);", addCartPage.addtoCart);


			} else {
				myLogger.error(" " + addCartPage.addtoCart + "Not Exist");
				utils.Utils.screenshotLocatorWrite(wd,addCartPage.addtoCart,"add_to_cart");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void check_add_ok_icon() {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(addCartPage.product_addcart_ok_added)) {
				System.out.println();
				myLogger.info("WebElement " + addCartPage.product_addcart_ok_added + "Exist");
				myLogger.info("icon ok displayed");
				// assertTrue(addCartPage.product_addcart_ok_added.getAttribute("value"));
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + addCartPage.product_addcart_ok_added + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				// assertNotEquals(productPage.elmSearchText.getAttribute("value"), keyword);
				// utils.Utils.screenShot(driver);
				myLogger.error("icon ok displayed");
				utils.Utils.screenshotLocatorWrite(wd, addCartPage.product_addcart_ok_added, "icon_ok");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void check_color_item(String color) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(addCartPage.check_color_item)) {
				System.out.println();
				myLogger.info("WebElement " + addCartPage.check_color_item + "Exist");
				myLogger.info("icon ok displayed");
				assertEquals(addCartPage.check_color_item.getText(), color);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + addCartPage.check_color_item + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				// assertNotEquals(productPage.elmSearchText.getAttribute("value"), keyword);
				// utils.Utils.screenShot(driver);
				myLogger.error("icon ok displayed");
				utils.Utils.screenshotLocatorWrite(wd, addCartPage.check_color_item, "color");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void check_quantity(String quantity) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(addCartPage.quantity)) {
				System.out.println();
				myLogger.info("WebElement " + addCartPage.quantity + "Exist");

				assertEquals(addCartPage.quantity.getText(), quantity);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + addCartPage.quantity + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(addCartPage.quantity.getText(), quantity);
				// utils.Utils.screenShot(driver);
				myLogger.error("icon ok displayed");
				utils.Utils.screenshotLocatorWrite(wd, addCartPage.quantity, "quantity");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void check_total_product_price(String totalProductPrice) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(addCartPage.totalProductsPrice)) {
				System.out.println();
				myLogger.info("WebElement " + addCartPage.totalProductsPrice + "Exist");

				assertEquals(addCartPage.totalProductsPrice.getText(), totalProductPrice);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + addCartPage.totalProductsPrice + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(addCartPage.totalProductsPrice.getText(), totalProductPrice);
				// utils.Utils.screenShot(driver);
				myLogger.error("icon ok displayed");
				utils.Utils.screenshotLocatorWrite(wd, addCartPage.check_color_item, "color");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void check_shipping_cost(String shippingcost) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(addCartPage.shippingCost)) {
				System.out.println();
				myLogger.info("WebElement " + addCartPage.shippingCost + "Exist");

				assertEquals(addCartPage.shippingCost.getText(), shippingcost);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + addCartPage.shippingCost + "Not Exist");

				utils.Utils.screenshotLocatorWrite(wd, addCartPage.check_color_item, "shipcost");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void continue_shoping_button() throws Throwable {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(addCartPage.continue_shopping_button)) {
				Actions actions = new Actions(wd);
				actions.moveToElement(addCartPage.continue_shopping_button).perform();
				 WebDriverWait wait = new WebDriverWait(wd, 10);
			        wait.until(ExpectedConditions.elementToBeClickable(addCartPage.continue_shopping_button));
			        addCartPage.continue_shopping_button.click();
				myLogger.info(" " + addCartPage.continue_shopping_button + "Exist");
				// assertEquals(addCartPage.addtoCart.getAttribute("value"), "+");

			} else {
				myLogger.error(" " + addCartPage.continue_shopping_button + "Not Exist");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void priview_check_button() {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(addCartPage.precheck_checkout_button)) {
				Actions actions = new Actions(wd);
				actions.moveToElement(addCartPage.precheck_checkout_button).perform();
				 WebDriverWait wait = new WebDriverWait(wd, 10);
			        wait.until(ExpectedConditions.elementToBeClickable(addCartPage.precheck_checkout_button));
			        addCartPage.precheck_checkout_button.click();
				myLogger.info(" " + addCartPage.precheck_checkout_button + "Exist");
				// assertEquals(addCartPage.addtoCart.getAttribute("value"), "+");

			} else {
				myLogger.error(" " + addCartPage.precheck_checkout_button + "Not Exist");
				utils.Utils.screenshotLocatorWrite(wd, addCartPage.precheck_checkout_button, "privie_check");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	/*
	 * public void precheck_check_button() throws Throwable {
	 *//**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 *//*
			 * try { if (Utils.existsElement(addCartPage.prechk_to_checkout_button)) {
			 * addCartPage.prechk_to_checkout_button.click(); myLogger.info(" " +
			 * addCartPage.prechk_to_checkout_button + "Exist");
			 * //assertEquals(addCartPage.addtoCart.getAttribute("value"), "+");
			 * 
			 * } else { myLogger.error(" " + addCartPage.prechk_to_checkout_button +
			 * "Not Exist"); Utils.screenshotLocatorWrite(wd,
			 * addCartPage.precheck_checkout_button, "pre_check"); } } catch
			 * (NoSuchElementException e) { e.printStackTrace();
			 * 
			 * } }
			 */

	public void check_box_click() throws Throwable {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(addCartPage.check_Conditions)) {
				Actions actions = new Actions(wd);
				actions.moveToElement(addCartPage.check_Conditions).perform();
				 WebDriverWait wait = new WebDriverWait(wd, 10);
			        wait.until(ExpectedConditions.elementToBeClickable(addCartPage.check_Conditions));
			       // addCartPage.precheck_checkout_button.click();
				addCartPage.check_Conditions.click();
				myLogger.info(" " + addCartPage.check_Conditions + "Exist");
				// assertEquals(addCartPage.addtoCart.getAttribute("value"), "+");

			} else {
				myLogger.error(" " + addCartPage.check_Conditions + "Not Exist");
				Utils.screenshotLocatorWrite(wd, addCartPage.check_Conditions, "final_checkout");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	
	
	public void click_mouse_over_item(String productname) {
		WebDriverWait wait = new WebDriverWait(wd, 5);
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText(productname)));
		wd.findElement(By.linkText(productname)).click();
	}
	
	/*
	 * public void check_item_click_add_cart(String product_name) {
	 * 
	 * new WebDriverWait(wd, 20).until(ExpectedConditions.elementToBeClickable(By.
	 * xpath("//h4[@class='product-name' and starts-with(., '" +item+
	 * "')]//following::div[2]/button[text()='ADD TO CART']"))).click();
	 * 
	 * }
	 */
	
	public static void AddtoCart(String[] additems) 
    {       
		//inStock
    List<WebElement> products=addCartPage.inStock;
    for(int i=0;i<products.size();i++)

    {
        
    String[] productname=products.get(i).getText().split("-");

    String frmtdname=productname[0].trim();

   
    List itemsneeded = Arrays.asList(additems);

    if(itemsneeded.contains(frmtdname))

    {

    //click on Add to cart  "//button[text() ='ADD TO CART']"

    wd.findElements(By.xpath("//div[@class='add-action']/button")).get(i).click();

    }
    }
    
    }
	
	public void view_cart_click() throws Throwable {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(addCartPage.view_my_cart)) {
				Actions actions = new Actions(wd);
				actions.moveToElement(addCartPage.view_my_cart).perform();
				 WebDriverWait wait = new WebDriverWait(wd, 10);
			        wait.until(ExpectedConditions.elementToBeClickable(addCartPage.view_my_cart));
			      addCartPage.view_my_cart.click();
				
				myLogger.info(" " + addCartPage.view_my_cart + "Exist");
				// assertEquals(addCartPage.addtoCart.getAttribute("value"), "+");

			} else {
				myLogger.error(" " + addCartPage.view_my_cart + "Not Exist");
				Utils.screenshotLocatorWrite(wd, addCartPage.view_my_cart, "view_checkout");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	 public String getItemInCartName() {
	      
	        String cart = wd.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div[2]/table/tbody/tr/td[2]/p/a")).getText();
	        return cart;
	    }

	  
	    public String getPriceOfItemInCart() {
	        String price = wd.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div[2]/table/tbody/tr/td[4]/span/span")).getText();
	        return price;
	    }

	    public void clickOnDelete() {
	        wd.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
	        wd.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div[2]/table/tbody/tr/td[7]/div/a/i")).click();
	        wd.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);

	    }

	    public boolean isEmpty() {
	        wd.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
	        boolean emptyAlert = wd.findElements(By.className("alert-warning")).size() > 0;
	        wd.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
	        return emptyAlert;
	    }
	    
	    public boolean alertSuccessMessagePresent() {
	        boolean alertPresent = wd.findElements (By.xpath("//*[@id=\"center_column\"]/p[1]")).size() > 0;
	        return alertPresent;
	    }



	
}
