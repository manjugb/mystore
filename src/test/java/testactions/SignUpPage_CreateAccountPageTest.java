package testactions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static utils.Constants.myLogger;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.runtime.Timeout;
import pageObjects.MyStore_SignUpPage_CreateAccount;
import utils.BrowserFactory;

public class SignUpPage_CreateAccountPageTest extends BrowserFactory {

	private MyStore_SignUpPage_CreateAccount CreateAccountPage = new MyStore_SignUpPage_CreateAccount();
    WebDriverWait wait;
	public SignUpPage_CreateAccountPageTest() {
		this.CreateAccountPage = new MyStore_SignUpPage_CreateAccount();
		PageFactory.initElements(wd, CreateAccountPage);
	}

	public void sign_link() {
		/**
		 * @author manjunath
		 * @description this method validates SignIn Link
		 */
		try {
			if (utils.Utils.existsElement(CreateAccountPage.sign_in_link)) {
				myLogger.info("WebElement " + CreateAccountPage.sign_in_link + "Exist");
				assertEquals(CreateAccountPage.sign_in_link.getText(), "Sign in");

				CreateAccountPage.sign_in_link.click();

				// myLogger.info(" " + keyword + "Found");
			} else {
				assertNotEquals(CreateAccountPage.sign_in_link.getText(), "Sign in");
				myLogger.info("WebElement " + CreateAccountPage.sign_in_link + "Not Exist");
				utils.Utils.screenshotLocatorWrite(wd, CreateAccountPage.sign_in_link, "SignInLink");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void verify_h3_title_create_account(String h3title) {
		/**
		 * @author manjunath
		 * @description this method validates SignIn Link
		 */
		try {
			if (utils.Utils.existsElement(CreateAccountPage.create_h3_title)) {
				myLogger.info("WebElement " + CreateAccountPage.create_h3_title + "Exist");
				assertEquals(CreateAccountPage.create_h3_title.getText(), h3title);

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + CreateAccountPage.create_h3_title + "Not Exist");
				assertNotEquals(CreateAccountPage.create_h3_title.getText(), h3title);
				utils.Utils.screenshotLocatorWrite(wd, CreateAccountPage.sign_in_link, "SignInLink");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void verify_label_create_account(String label) {
		/**
		 * @author manjunath
		 * @description this method validates label of email
		 */
		try {
			if (utils.Utils.existsElement(CreateAccountPage.lable_email)) {
				myLogger.info("WebElement " + CreateAccountPage.lable_email + "Exist");
				assertEquals(CreateAccountPage.lable_email.getText(), label);

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + CreateAccountPage.lable_email + "Not Exist");
				assertNotEquals(CreateAccountPage.create_h3_title.getText(), label);
				utils.Utils.screenshotLocatorWrite(wd, CreateAccountPage.lable_email, "EmailLable");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void enter_email_text(String email) {
		/**
		 * @author manjunath
		 * @description this method validates email text field
		 */
		try {
			if (utils.Utils.existsElement(CreateAccountPage.emailText)) {
				CreateAccountPage.emailText.clear();
				CreateAccountPage.emailText.sendKeys(email);
				myLogger.info("WebElement " + CreateAccountPage.emailText + "Exist");
				assertEquals(CreateAccountPage.emailText.getAttribute("value"), email);

				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info("WebElement " + CreateAccountPage.emailText + "Not Exist");
				//assertNotEquals(CreateAccountPage.emailText.getAttribute("value"), email);
				utils.Utils.screenshotLocatorWrite(wd, CreateAccountPage.emailText, "EnterEmailText");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}

	}
	


	public void create_account_email_validation(String email) throws InterruptedException {
		
		try {
		if (utils.Utils.existsElement(CreateAccountPage.createButton)) {
			enter_email_text(email);
			CreateAccountPage.createButton.submit();
			//MyStoreCommonPageTest.waitForPageToLoad();
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			utils.Utils.screenshotLocatorWrite(wd, "create_account_exception");

		}
	}
	
	public void verify_error_msg(String errmsg) throws InterruptedException {
		try {
		if (utils.Utils.existsElement(CreateAccountPage.create_account_err)) {
			assertEquals(CreateAccountPage.create_account_err.getText(),errmsg);
			//assertFalse(CreateAccountPage.h1_create_account_title.getText(),false);
			//assertFalse(CreateAccountPage.h3_personal_info_title.getText(),false);
			utils.Utils.screenshotLocatorWrite(wd, CreateAccountPage.create_account_err, "create_account_err");
				
			} else {
				utils.Utils.screenshotLocatorWrite(wd, "create_success");
				
			}
			
		
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			utils.Utils.screenshotLocatorWrite(wd,"screen_exception");
		}
	}
	

	/*
	 * public void create_account_sign_up(String email) {
	 * 
	 * try { if (utils.Utils.existsElement(CreateAccountPage.emailText.getText().isEmpty()) {
	 * myLogger.info("WebElement " + CreateAccountPage.emailText + "Exist");
	 * enter_email_text(email);
	 * 
	 * 
	 * 
	 * // myLogger.info(" " + keyword + "Found"); } else {
	 * myLogger.info("WebElement " + CreateAccountPage.emailText + "Not Exist");
	 * utils.Utils.screenshotLocatorWrite(wd,
	 * CreateAccountPage.emailText,"CreateButton"); } } catch
	 * (NoSuchElementException e) { e.printStackTrace();
	 * 
	 * } }
	 */

}
