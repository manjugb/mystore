package testactions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static utils.Constants.myLogger;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import pageObjects.MyStoreProductPage;
import utils.BrowserFactory;
import utils.Utils;

public class MyStoreProductPageTest extends BrowserFactory {
	/**
	 * @author manjunath {
	 * @summary} This class defines the methods related to Searching of using Search
	 *           Tab
	 */

	private static MyStoreProductPage productPage = new MyStoreProductPage();

	public MyStoreProductPageTest() {
		this.productPage = new MyStoreProductPage();
		PageFactory.initElements(wd, productPage);
	}

	public void enter_key(String keyword) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(productPage.elmSearchText)) {
				System.out.println();
				// productPage.elmSearchText.clear();
				productPage.elmSearchText.click();
				productPage.elmSearchText.sendKeys(keyword);
				myLogger.info("WebElement " + productPage.elmSearchText + "Exist");
				assertEquals(productPage.elmSearchText.getAttribute("value"), keyword);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + productPage.elmSearchText + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(productPage.elmSearchText.getAttribute("value"), keyword);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, productPage.elmSearchText, "SearchTabText");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void submit_search() throws Throwable {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(productPage.butSearch)) {
				productPage.butSearch.click();
				myLogger.info(" " + productPage.butSearch + "Exist");

			} else {
				myLogger.info(" " + productPage.butSearch + "Not Exist");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void verifySearchKey(String strResults) throws Throwable {
		try {
			if (Utils.existsElement(productPage.searchResultsText)) {
				if (wd.getPageSource().contains(strResults)) {
					myLogger.info(" " + productPage.searchResultsText.getText() + "Exist");
					myLogger.info("Text: " + strResults + " is present. ");
					assertEquals(productPage.searchResultsText.getText(), strResults);
				} else {
					myLogger.info(" " + productPage.searchResultsText + "Not Exist");
					myLogger.info("Text: " + strResults + " is not present. ");
					assertNotEquals(productPage.searchResultsText.getText(), strResults);
					utils.Utils.screenshotLocatorWrite(wd, productPage.elmSearchText, "SearchTabText");
				}

			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public void grid_list_click() throws Throwable {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(productPage.elm_list_grid_click)) {
				productPage.elm_list_grid_click.click();
				myLogger.info(" " + productPage.elm_list_grid_click + "Exist");

			} else {
				myLogger.info(" " + productPage.elm_list_grid_click + "Not Exist");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	public int check_product_in_stock(int count) {
		List<WebElement> InStockItem = productPage.inStock;
		InStockItem.size();
		if (InStockItem.size() > 0) {
			for (int i = 0; i < InStockItem.size(); i++) {
				myLogger.info(InStockItem.get(i));

			}
		} else {
			wd.navigate().back();
		}
		return count;
	}

	public List<WebElement> getCartProductsQty() {
		return productPage.inStock;
	}

	public static void AddtoCart(WebDriver Driver, String[] additems) {

		List<WebElement> products = productPage.inStock;
		for (int i = 0; i < products.size(); i++)

		{

			String[] productname = products.get(i).getText().split("-");

			String frmtdname = productname[0].trim();

			// format it to get actual vegetable name

			// convert array into array list for easy search

			// check whether name you extracted is present in arrayList or not-
			List itemsneeded = Arrays.asList(additems);

			if (itemsneeded.contains(frmtdname))

			{

				// click on Add to cart

				Driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();

			}
		}

	}

	public void click_product(String productname) {
		List<WebElement> product = new WebDriverWait(wd, 30).until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//ul[@class='product_list grid row']")));
		System.out.println("prdoduct=" + product.size());
		for (int i = 0; i < product.size(); i++) {
			String name = product.get(i).getText();
			myLogger.info("NAME is" + name);

			if (name.contains(productname)) {
				wd.findElement(By.linkText(productname)).click();
			}
		}
	}

	// ht title
	public void verify_h1_title(String h1title) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(productPage.h1title)) {

				myLogger.info("WebElement " + productPage.h1title + "Exist");
				assertEquals(productPage.h1title.getText(), h1title);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + productPage.h1title + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(productPage.h1title.getText(), h1title);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, productPage.h1title, "h1title");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	// condition
	public void verify_condition_item(String condition) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(productPage.checkItemCondtion)) {

				myLogger.info("WebElement " + productPage.checkItemCondtion + "Exist");
				assertEquals(productPage.checkItemCondtion.getText(), condition);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + productPage.checkItemCondtion + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(productPage.checkItemCondtion.getText(), condition);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, productPage.checkItemCondtion, "h1title");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	// product
	public void verify_product_price(String prod_price) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(productPage.productPrice)) {

				myLogger.info("WebElement " + productPage.productPrice + "Exist");
				assertEquals(productPage.productPrice.getText(), prod_price);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + productPage.productPrice + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(productPage.productPrice.getText(), prod_price);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, productPage.productPrice, "prod_price");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	// discount
	public void verify_prod_discount(String prod_discount) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (productPage.prodDiscount.isDisplayed()) {

				myLogger.info("WebElement " + productPage.prodDiscount + "Exist");
				assertEquals(productPage.prodDiscount.getText(), prod_discount);
				// myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.error(" " + productPage.prodDiscount + "Not Exist");
				// myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(productPage.prodDiscount.getText(), prod_discount);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, productPage.prodDiscount, "h1title");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	// quantity increase
	public void increase_quantity() {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(productPage.increase_quantity)) {
				productPage.increase_quantity.click();
				myLogger.info(" " + productPage.increase_quantity + "Exist");
				//assertEquals(productPage.increase_quantity.getAttribute("value"), "+");

			} else {
				myLogger.error(" " + productPage.increase_quantity + "Not Exist");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	// quantity dicrease
	public void dicrease_quantity() throws Throwable {
		/**
		 * @author manjunath
		 * @description this method validates the button and able to click
		 */
		try {
			if (Utils.existsElement(productPage.dicrease_quantity)) {
				productPage.dicrease_quantity.click();
				myLogger.info(" " + productPage.dicrease_quantity + "Exist");
				assertEquals(productPage.dicrease_quantity.getAttribute("value"), "-");

			} else {
				myLogger.error(" " + productPage.increase_quantity + "Not Exist");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}

	// Add to Cart
	
	//using data tables
	
	/*
	 * public void enterData(DataTable table){ //Initialize data table List<List>
	 * data = table.raw();
	 */
}
