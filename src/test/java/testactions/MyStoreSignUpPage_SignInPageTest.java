package testactions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static utils.Constants.myLogger;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import pageObjects.MyStore_SignUpPage_SignInPage;
import utils.BrowserFactory;
import utils.Utils;

public class MyStoreSignUpPage_SignInPageTest extends BrowserFactory {
	public static final String ANSI_RED_TEXT = "\033[31m";
	public static final String ANSI_GREEN_TEXT = "\033[32;1;2m";
	private MyStore_SignUpPage_SignInPage signPage = new MyStore_SignUpPage_SignInPage();
	//private CommonPageTest comPageTest = new CommonPageTest();
	SoftAssert softAssertion=new SoftAssert(); 

	public MyStoreSignUpPage_SignInPageTest() {
		this.signPage = new MyStore_SignUpPage_SignInPage();
		PageFactory.initElements(wd, signPage);
	}
	
	
	public void enter_email_signIn(String email) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(signPage.email)) {
				System.out.println();
				// searchPage.elmSearchText.clear();
				signPage.email.click();
				signPage.email.sendKeys(email);
				myLogger.info("WebElement " + signPage.email + "Exist");
				assertEquals(signPage.email.getAttribute("value"), email);
				//myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + signPage.email+ "Not Exist");
				//myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(signPage.email.getAttribute("value"), email);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, signPage.email, "SignInPageEmail");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	
	public void enter_pswrd_signIn(String pswrd) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(signPage.paswrod)) {
				System.out.println();
				// searchPage.elmSearchText.clear();
				signPage.paswrod.click();
				signPage.paswrod.sendKeys(pswrd);
				myLogger.info("WebElement " + signPage.paswrod + "Exist");
				assertEquals(signPage.paswrod.getAttribute("value"), pswrd);
				//myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + signPage.paswrod+ "Not Exist");
				//myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(signPage.paswrod.getAttribute("value"), pswrd);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, signPage.paswrod, "SignInPswrd");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void submit_signIn() {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(signPage.signIn_but)) {
				System.out.println();
				// searchPage.elmSearchText.clear();
				MyStoreCommonPageTest.waitForPageToLoad();
				Actions actions = new Actions(wd);
				actions.moveToElement(signPage.signIn_but).perform();
				 WebDriverWait wait = new WebDriverWait(wd, 10);
			        wait.until(ExpectedConditions.elementToBeClickable(signPage.signIn_but));
			        signPage.signIn_but.click();
				//MyStoreCommonPageTest.waitForPageToLoad();
				
				myLogger.info("WebElement " + signPage.signIn_but + "Exist");
				
				//myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + signPage.signIn_but + "Not Exist");
				utils.Utils.screenshotLocatorWrite(wd, signPage.signIn_but, "SearchTabText");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void login_signIn_error(String errmsg) {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(signPage.login_err)) {
				System.out.println();
				myLogger.info("WebElement " + signPage.login_err + "Exist");
				assertEquals(signPage.login_err.getText(), errmsg);
				//myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + signPage.login_err+ "Not Exist");
				//myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(signPage.login_err.getText(), errmsg);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, signPage.login_err, "SignInError");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

		}
	}
	
	public void check_user_success(String name) {
		/**
		 * @author manjunath
		 * @description this method validates name after successful signin
		 */
		try {
			if (Utils.existsElement(signPage.check_user_success)) {
				System.out.println();
				myLogger.info("WebElement " + signPage.check_user_success + "Exist");
				assertEquals(signPage.check_user_success.getText(), name);
				//myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + signPage.paswrod+ "Not Exist");
				//myLogger.info(" " + keyword + "Not Found");
				assertNotEquals(signPage.check_user_success.getText(), name);
				// utils.Utils.screenShot(driver);
				utils.Utils.screenshotLocatorWrite(wd, signPage.check_user_success, "check_user_success");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			utils.Utils.screenshotLocatorWrite(wd, signPage.check_user_success, "check_user_success");
		}
	}
	
	public void submit_logout() {
		/**
		 * @author manjunath
		 * @description this method validates the entering of text and element
		 */
		try {
			if (Utils.existsElement(signPage.logout)) {
				System.out.println();
				// searchPage.elmSearchText.clear();
				Actions actions = new Actions(wd);
				actions.moveToElement(signPage.logout).perform();
				 WebDriverWait wait = new WebDriverWait(wd, 10);
			        wait.until(ExpectedConditions.elementToBeClickable(signPage.logout));
			        signPage.logout.click();
				signPage.logout.click();
				myLogger.info("WebElement " + signPage.logout + "Exist");
				
				//myLogger.info(" " + keyword + "Found");
			} else {
				myLogger.info(" " + signPage.logout + "Not Exist");
				utils.Utils.screenshotLocatorWrite(wd, signPage.logout, "logout");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			utils.Utils.screenshotLocatorWrite(wd, signPage.logout, "logout");

		}
	}
	
	public void verify_error_msg(String errmsg) {
		try {
			if (Utils.existsElement(signPage.login_error)) {
			softAssertion.assertTrue(signPage.login_error.toString().contains(errmsg));
			myLogger.info(ANSI_GREEN_TEXT + "" + errmsg + "" + "Found");
			// test.log(Status.PASS, "Search Key Found");
		} else {
			softAssertion.assertFalse(signPage.login_error.toString().contains(errmsg));
			myLogger.info(ANSI_GREEN_TEXT + "" + errmsg + "" + "Not Found");
			// test.log(Status.FAIL, "Search Key NOT Found");

		}
		
	} catch (NoSuchElementException e) {
		e.printStackTrace();
		utils.Utils.screenshotLocatorWrite(wd, signPage.logout, "logout");

	}
	}
	
	public WebElement successfullyCreatedAccount() {
		return utils.Utils.waitForElementPresence(wd, By.xpath("//p[contains(text(), \"Welcome to your account.\")]"), 30);
	}
	


}
