package testactions;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.MyStoreCommonPage;
import utils.BrowserFactory;

public class MyStoreCommonPageTest extends BrowserFactory{
	/** {@summary} this class having maintenance  future purpose 
	 * 
	 */
	WebDriverWait wait;
	
	public MyStoreCommonPage comPag = new MyStoreCommonPage();

	public MyStoreCommonPageTest() {
		this.comPag = new MyStoreCommonPage();
		PageFactory.initElements(wd, comPag);
	}
	
	boolean isElementPresent(By by) {
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch(TimeoutException e) {
			return false;
		}
		return true;
	}

	
	public boolean pageHasText(String text) {
	    return isElementPresent(By.xpath("//*[contains(text(), '" + text + "')]"));
    }
	
	boolean isPresent(By by) {
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch(TimeoutException e) {
			return false;
		}
		return true;
	}

	WebElement getElementByCSS(String cssSelector) {
	    return wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
    }

    WebElement getElementByXPath(String xPathQuery) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathQuery)));
    }

    WebElement getElementById(String id) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }

    WebElement getElementByTagName(String tagName) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(tagName)));
    }

    
	void executeJavaScript(String jSCode) {
		((JavascriptExecutor)wd).executeScript(jSCode);
	}

	public static void waitForPageToLoad() {
		int retryAttempts = 10, counter = 0;
		long delay = 1000;
		String jsCode = "try {" +
				"if (document.readyState !== 'complete') { return false; }" +
				"if (window.jQuery) { if (window.jQuery.active !== 0) { return false; } }" +
				"return true;" +
				"} catch (ex) { return false; }";
		while(counter < retryAttempts) {
			if(!(Boolean)((JavascriptExecutor)wd).executeScript(jsCode)) {
				System.out.println("Page is busy ...");
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			counter++;
		}
	}
	
	
	
	



}
