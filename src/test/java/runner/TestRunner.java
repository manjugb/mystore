package runner;
import java.io.File;

import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features/"},
        //format = {"json:target/cucumber.json", "html:target/site/cucumber-pretty"},
        glue = {"steps"},
        //~ will skip the features with that specific tag
       // tags = {"@web,@SignUP_SignIn"},
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:report/report.html"},
        monochrome = true
        
        
)

public class TestRunner{

     //writes in the report
    @AfterClass
    public static void writeExtentReport() {
        try {
            //Reporter.loadXMLConfig(new File(utils.ConfigFileReader.getInstance().getreportConfigPath()));
            Reporter.loadXMLConfig(utils.ConfigFileReader.getInstance().getreportConfigPath());
	        //Reporter.loadXMLConfig("/config/extent-Config.xml");
            Reporter.setSystemInfo("user", System.getProperty("user.name"));
            Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
            Reporter.setSystemInfo("Machine", "Ubuntu 20.04" + "64 Bit");
            Reporter.setSystemInfo("Selenium", "3.141.59");
            Reporter.setSystemInfo("Maven", "3.7.0");
            Reporter.setSystemInfo("Java Version", "11");
            Reporter.setTestRunnerOutput("My Store Page Output");
        }catch(Exception e){
            System.out.println("Error: " + e);
        }


    }

  
}

