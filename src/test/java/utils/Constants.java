package utils;

import org.apache.log4j.Logger;


public class Constants {
    public static final String userDirPath = System.getProperty("user.dir");
    public static final Logger myLogger = Logger.getLogger(Constants.class);
    
}
