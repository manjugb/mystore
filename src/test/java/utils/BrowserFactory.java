package utils;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;



public class BrowserFactory {

    public static WebDriver wd=null;

    public BrowserFactory(){
    }

    public static WebDriver getDriver(String url,String browser){
        setUpDriver(browser);
        wd.get(url);
		wd.manage().window().maximize();
		wd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
        return wd;
    }

    public static void setUpDriver(String browser) {
        if(wd == null && browser.equalsIgnoreCase("Chrome")){
            WebDriverManager.getInstance(ChromeDriver.class).setup();
            /*ChromeOptions options = new ChromeOptions();
            options.addArguments("headless");
            options.addArguments("window-size=1280x800");
            options.addArguments("no-sandbox");
            options.addArguments("–disable-dev-shm-usage");
            options.addArguments("start-maximized");
            options.addArguments("--disable-gpu");
           options.addArguments("--disable-setuid-sandbox");
            wd = new ChromeDriver(options);*/
            wd = new ChromeDriver();
            wd.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            wd.manage().deleteAllCookies();
            
            
        }else if(wd == null && browser.equalsIgnoreCase("Firefox")) {
            WebDriverManager.getInstance(FirefoxDriver.class).setup();
            wd = new FirefoxDriver();
            wd.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            wd.manage().deleteAllCookies();
            
        }
    }

    public static void closeBrowser(){
        wd.quit();
    }

	/*
	 * public static void takeScreenShot(Scenario scenario){ byte[]
	 * screenBytes=((TakesScreenshot) wd).getScreenshotAs(OutputType.BYTES);
	 * ((Object) scenario).embed(screenBytes,"image/png"); }
	 */

	public boolean isPageLoaded() {
		// TODO Auto-generated method stub
		return false;
	}

	
	
	public WebDriver getDriver() {
		return wd;

	}

	public void destroyDriver() {
		if (wd == null) {
			return;
		}
		wd.quit();
		wd = null;
	}


	

}
