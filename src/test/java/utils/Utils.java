
package utils;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;
import static utils.Constants.myLogger;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.qameta.allure.Attachment;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

public  class Utils {

	private static final int WAIT_TIMEOUT = 15;
	private static final int SHORT_TIMEOUT = 3;
	private static final long DEFAULT_DELAY = 3000;
	protected static final long SHORT_DELAY = 1000;
	private static final long CHAR_TYPE_DELAY = 100;
    WebDriver driver;
	static WebDriverWait wait;
	private WebDriverWait shortWait;
	public static String dateTime = new SimpleDateFormat("yyyyMMdd-hha").format(new Date());
	protected final static String image_path = ".//images//" + dateTime + ".png";
	

    /*************************************************************
     Description: Checks if an element exists using locator
     Parameters: webdriver, locator
     Return: boolean
     ***********************************************************/
    public static boolean existsElement (WebDriver driver, By locator){
        try {
            driver.findElement(locator);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
	
	public static boolean existsElement (WebElement element){
        try {
            if (!element.isDisplayed()) {
            	myLogger.info(" Element "+element+ "Not Found");
            	throw new IllegalStateException("" +element+" is not displayed!");
            	//myLogger.info(" Element "+element+ "Not Found");
            }else {
            	myLogger.info("Element"+element+ " Found");
            	
            	
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
	
	

    /*************************************************************
     Description: Checks if an element is visible or not, so for instance an element that is in the DOM
     but may be or not present depending on conditions
     Parameters: webdriver, time in seconds to wait, locator
     Return: element if found, if not returns null
     ***********************************************************/
    public static WebElement visibleOrNot(WebDriver driver, int timeToWait, By locator) {
        WebElement element = null;
        try {
            element = new FluentWait<>(driver).
                    withTimeout(Duration.ofSeconds(timeToWait)).
                    pollingEvery(Duration.ofSeconds(1)).
                    ignoring(NotFoundException.class).ignoring(NoSuchElementException.class).
                    until(visibilityOfElementLocated(locator));
        } catch (TimeoutException ex) {}
        return element;
    }

    /*************************************************************
     Description: Clicks an element using Javascript
     Parameters: driver, webelement to click
     Return: NA
     ***********************************************************/
    public static void clickElementThroughJavaScript(WebDriver driver, WebElement element){
        //To Click on elements which may not be visible on screen
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
        
    }

    /*************************************************************
     Description: Takes an screenshot for a specific locator and save it
     Paramaters: driver as webdriver, locator as webelement, image name as String
     Return: NA
     ***********************************************************/
    public static void screenshotLocatorWrite(WebDriver driver, WebElement element, String imageName){
        try {
            Screenshot screenshot = new AShot().takeScreenshot(driver, element);
            ImageIO.write(screenshot.getImage(), "PNG",
                    new File(System.getProperty("user.dir") + "/images/" + imageName + ".png"));
        }catch (IOException e){
            System.out.println("OPTA: Not able to take screenshot locator - " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    /*************************************************************
    Description: Takes an screenshot for a specific locator and save it
    Paramaters: driver as webdriver, locator as webelement, image name as String
    Return: NA
    ***********************************************************/
   public static void screenshotLocatorWrite(WebDriver driver,String imageName){
       try {
           Screenshot screenshot = new AShot().takeScreenshot(driver);
           ImageIO.write(screenshot.getImage(), "PNG",
                   new File(System.getProperty("user.dir") + "/images/" + imageName + ".png"));
       }catch (IOException e){
           System.out.println("OPTA: Not able to take screenshot locator - " + e.getMessage());
           e.printStackTrace();
       }
   }


    /*************************************************************
     Description: compares two images, one saved and another one taken from selector
     Parameters: driver as webdriver, saved images as String, locator as web element
     Return: images difference as boolean
     ***********************************************************/
    public static boolean compareImages(WebDriver driver, String savedImage, WebElement locator){
        try {
            //get the saved image, the expected one
            BufferedImage expectedSatellite =
                    ImageIO.read(new File(System.getProperty("user.dir")+image_path+ savedImage +".png"));

            //take the new screenshot
            Screenshot screenshot = new AShot().takeScreenshot(driver, locator);
            BufferedImage actualSatellite = screenshot.getImage();

            //compare both images
            ImageDiffer imgDiff = new ImageDiffer();
            ImageDiff diff = imgDiff.makeDiff(expectedSatellite,actualSatellite);

            return diff.hasDiff();

        }catch (IOException e){
            System.out.println("OPTA: Not able to compare images - " + e.getMessage());
            e.printStackTrace();

            return true;
        }
    }

    /*************************************************************
     Description: Attach an screenshot, with allure attachment tag
     Paramaters: webdriver
     Return: NA
     ***********************************************************/
    @Attachment( type = "image/png")
    public static  byte[] screenShot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }


  


    /*************************************************************
     Description: Wait for an element to be changed dinamically for any javascript
     Paramaters: webdriver, the expected value once it changed, the Xpath a xpath
     the time to wait for it to change
     Return: boolean of result
     ***********************************************************/
    public static String checkValueChanged(WebDriver driver, String expectedValue, String path, int waitTime) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String elementValue = (String) js
                .executeScript("return document.evaluate(\""+path+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value;");

        while (!(elementValue.equals(expectedValue)) && (waitTime>0)) {
            Thread.sleep(1000);
            waitTime--;
            elementValue = (String) js
                    .executeScript("return document.evaluate(\""+path+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value;");
        }
        return elementValue;
    }



    public static WebElement waitToBeClickable(WebDriver driver, By selector, int waitInterval) {
		WebElement element = (new WebDriverWait(driver, waitInterval)).until(ExpectedConditions.elementToBeClickable(selector));
		return element;
	}
	
    public static WebElement waitForElementPresence(WebDriver driver, By selector, int waitInterval) {
		WebElement element = (new WebDriverWait(driver, waitInterval)).until(ExpectedConditions.presenceOfElementLocated(selector));
		return element;
	}
    
    

    



}
