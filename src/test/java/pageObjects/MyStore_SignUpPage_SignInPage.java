package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MyStore_SignUpPage_SignInPage {

@FindBy(how=How.XPATH,using="//input[@id='email']")
public WebElement email;

@FindBy(how=How.XPATH,using="//input[@id='passwd']")
public WebElement paswrod;

@FindBy(how=How.XPATH,using="//body[@id='authentication']/div[@id='page']/div[@class='columns-container']/div[@id='columns']/div[@class='row']/div[@id='center_column']/div[@class='row']/div[@class='col-xs-12 col-sm-6']/form[@id='login_form']/div[@class='form_content clearfix']/p[@class='submit']/button[@id='SubmitLogin']/span[1]")
public WebElement signIn_but;

@FindBy(how=How.XPATH,using="//body[@id='authentication']/div[@id='page']/div[@class='columns-container']/div[@id='columns']/div[@class='row']/div[@id='center_column']/div[1]")
public WebElement login_err;
//*[contains(text(), '" + text + "')
@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/nav[1]/div[1]/a[1]/span[1]")
public WebElement check_user_success;

@FindBy(how=How.XPATH,using="//a[@class='logout']")
public WebElement logout;

@FindBy(how=How.CSS,using="html body#authentication.authentication.hide-left-column.hide-right-column.lang_en div#page div.columns-container div#columns.container div.row div#center_column.center_column.col-xs-12.col-sm-12 div.row div.col-xs-12.col-sm-6 form#create-account_form.box div.form_content.clearfix div#create_account_error.alert.alert-danger ol li")
public WebElement login_error;



}
