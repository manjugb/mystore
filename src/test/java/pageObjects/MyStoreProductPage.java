package pageObjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * 
 * @author manjunath
 * @description this class defines web elements of the myStore page
 * @features search Page
 */
public class MyStoreProductPage {
	@FindBy(how = How.XPATH, using = "//ul[@class='product_list grid row']")
	public List<WebElement> inStock;
	// searchTab
	@FindBy(how = How.XPATH, using = "//input[@id='search_query_top']")
	public WebElement elmSearchText;
	// searchbutton
	@FindBy(how = How.XPATH, using = "//form[@id='searchbox']//button[@type='submit']")
	public WebElement butSearch;
	// searchResults
	@FindBy(how = How.XPATH, using = "//span[@class='heading-counter']")
	public WebElement searchResultsText;
//Product Tab

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'List')]")
	public WebElement elm_list_grid_click;
	// grid


	// h1 title
	@FindBy(how = How.XPATH, using = "//h1[@itemprop='name']")
	public WebElement h1title;

	// condition of item new or old
	@FindBy(how = How.XPATH, using = "//p[@id='product_condition']")
	public WebElement checkItemCondtion;

	// price
	@FindBy(how = How.XPATH, using = "//span[@id='our_price_display']")
	public WebElement productPrice;

	// discount
	@FindBy(how = How.XPATH, using = "//span[@id='old_price_display']")
	public WebElement prodDiscount;

	// Decrease quantity
	@FindBy(how = How.XPATH, using = "//a[@class='btn btn-default button-minus product_quantity_down']//span")
	public WebElement dicrease_quantity;

	// increase quantity
	@FindBy(how = How.XPATH, using = "//a[@class='btn btn-default button-plus product_quantity_up']//span")
	public WebElement increase_quantity;


}