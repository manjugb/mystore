package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * 
 * @author manjunath
 * {@summary} this class defines SignIn related WebElements.
 *
 */

public class MyStore_SignUpPage_CreateAccount {

	@FindBy(how=How.XPATH,using="//a[@class='login']")
	public WebElement sign_in_link;
//Create Account	
	//h3 title
	@FindBy(how=How.XPATH,using="//h3[contains(text(),'Create an account')]")
	public WebElement create_h3_title;
	//Email Text
	@FindBy(how=How.XPATH,using="//input[@id='email_create']")
	public WebElement emailText;
	//email lable
	@FindBy(how=How.XPATH,using="//label[@for='email_create']")
	public WebElement lable_email;
	//create account link
	@FindBy(how=How.XPATH,using="//body[@id='authentication']/div[@id='page']/div[@class='columns-container']/div[@id='columns']/div[@class='row']/div[@id='center_column']/div[@class='row']/div[@class='col-xs-12 col-sm-6']/form[@id='create-account_form']/div[@class='form_content clearfix']/div[@class='submit']/button[@id='SubmitCreate']/span[1]") 
	public WebElement createButton;
	
	//email error empty text 
	@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/ol[1]/li[1]")
	public WebElement create_account_err;
	//div[@id='create_account_error']
	//h3 link : personal infor
	@FindBy(how=How.XPATH,using="//h1[@class='page-heading']")
	public WebElement h1_create_account_title; 
	@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/form[1]/div[1]/h3[1]")
	public WebElement h3_personal_info_title;
	
	@FindBy(how=How.XPATH,using="//form[@id='account-creation_form']")
	public WebElement account_create_form; //form[@id='account-creation_form']
	
}
