package pageObjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MyStoreAddCartPage {
	// add to cart
	@FindBy(how = How.CSS, using = "#add_to_cart")
	public WebElement addtoCart;
	
	
	@FindBy(how = How.XPATH, using = "//ul[@class='product_list grid row']")
	public List<WebElement> inStock;
//While Product Cart addition
	// product icon ok and message
	@FindBy(how = How.XPATH, using = "//i[@class='icon-ok']")
	public WebElement product_addcart_ok_added;
	

	// color
	@FindBy(how = How.XPATH, using = "//span[@id='layer_cart_product_attributes']")
	// quantity added form
	public WebElement check_color_item;
	@FindBy(how = How.XPATH, using = "//span[@id='layer_cart_product_quantity']")
	public WebElement quantity;
	// Total Price
	@FindBy(how = How.XPATH, using = "//span[@class='ajax_block_products_total']")
	public WebElement totalProductsPrice;
	// Shipping Cost
	@FindBy(how = How.XPATH, using = "//span[@class='ajax_cart_shipping_cost']")
	public WebElement shippingCost;

	// Continue Shopping
	@FindBy(how = How.LINK_TEXT, using = "Continue shopping")
	public WebElement continue_shopping_button;

	// precheck checkout
	@FindBy(how = How.LINK_TEXT, using = "Proceed to checkout")
	public WebElement precheck_checkout_button;

	@FindBy(how = How.LINK_TEXT, using = "Proceed to checkout")
	public WebElement prechk_to_checkout_button;

	@FindBy(how = How.XPATH, using = "//a[@class='button btn btn-default standard-checkout button-medium']//span[contains(text(),'Proceed to checkout')]")
	public WebElement final_to_checkout_button;

	@FindBy(how = How.XPATH,using="//input[@id='cgv']")
	public WebElement check_Conditions;
	
	@FindBy(how = How.LINK_TEXT, using = "View my shopping cart")
	public WebElement view_my_cart;

}
