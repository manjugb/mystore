package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MyStoreSignUpPage_Register_PersonalInfo {
@FindBy(how=How.XPATH,using="//input[@id='customer_firstname']")
public WebElement elmfirstname;
@FindBy(how=How.XPATH,using="//input[@id='customer_lastname']")
public WebElement elmlastname;
@FindBy(how=How.XPATH,using="//input[@id='passwd']")
public WebElement elmpswrd;
@FindBy(how=How.XPATH,using="//input[@id='firstname']")
public WebElement customer_firstname;
@FindBy(how=How.XPATH,using="//input[@id='lastname']")
public WebElement customer_lastname;
@FindBy(how=How.XPATH,using="//input[@id='address1']")
public WebElement customer_address;
@FindBy(how=How.XPATH,using="//input[@id='city']")
public WebElement customer_city;
@FindBy(how=How.XPATH,using="//select[@id='id_state']")
public WebElement select_state;
@FindBy(how=How.XPATH,using="//input[@id='postcode']")
public WebElement postcode;
@FindBy(how=How.XPATH,using="//input[@id='phone_mobile']")
public WebElement mobile_phone;
@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/form[1]/div[4]/button[1]")
public WebElement Register;
@FindBy(how=How.XPATH,using="//div[@class='alert alert-danger']")
public WebElement register_error;






}
